# AmadorUAVs Sim Infrastructure #
This repository contains all the source code for running the AmadorUAVs simulation infrastructure.

It includes a base docker-compose stack containing a PX4 SITL simulation container,
and a mavlink router to allow for multiple connections. It also includes a full
multi-drone stack for testing antenna tracker (one drone with a mounted gimbal is
treated as an antenna tracker) as well as other purposes. (The full drone stack
does not include a mavlink router.)

All the files for these stacks are located in their respective folders.

See the [wiki](https://gitlab.com/amadoruavs/sim/wikis/home) for more information on
how to run, edit, etc. Check out the [knowledge base](https://gitlab.com/amadoruavs/knowledge-base).
