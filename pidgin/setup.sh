# This handy script will install deps for Debian, Fedora and Arch Linux
# based systems.

# Perhaps the if then statements are stupid, but I really don't
# know a better way to do this.
# Oh, and if a bash dev is reading this, can your language PLEASE
# GET SOME NORMAL IF STATEMENTS THAT MAKE SENSE THANK YOU.
# - Vincent Wang, 2019-06-12

if [ -f /usr/bin/pacman ];
then
    # We're on an Arch based system, use pacman to install
    # Update full system first
    echo "Arch Linux-based system detected, using pacman..."
    echo "Updating system..."
    sudo pacman -Syu

    # get pip (python preinstalled)
    echo "Installing pip..."
    sudo pacman -S python-pip

    # Install Docker and Docker Compose
    echo "Installing Docker and Docker Compose..."
    sudo pacman -S docker docker-compose

    # Install main GStreamer packages
    echo "Installing GStreamer packages..."
    sudo pacman -S gstreamer && sudo pacman -S gst
    sudo pacman -S gst-libav gst-plugins-bad gst-plugins-bad gst-plugins-base gst-plugins-good gst-plugins-ugly gst-plugins-bad 

    echo "Done installing deps! Have a nice day :)"
elif [ -f /usr/bin/apt ];
then
    # We're on a Debian based system, use apt-get to install
    # Update full system first
    echo "Debian-based system detected, using apt-get..."
    echo "Updating system..."
    sudo apt update && sudo apt upgrade -y

    # get python3 (just in case) and pip
    echo "Installing pip..."
    sudo apt install python3 python3-pip

    # Install Docker and Docker Compose (using system repo, may change later)
    echo "Installing Docker and Docker Compose..."
    curl https://get.docker.com | sh
    sudo -H pip3 install docker-compose

    # Install main GStreamer packages
    echo "Installing GStreamer packages..."
    sudo apt-get install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-doc gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-gtk3 gstreamer1.0-qt5 gstreamer1.0-pulseaudio

    echo "Done installing deps! Have a nice day :)"
elif [ -f /usr/bin/dnf ];
then
    # We're on a Fedora based system, use dnf to install
    # Update full system first
    echo "Fedora-based system detected, using dnf..."
    echo "Updating system..."
    sudo dnf upgrade --refresh

    # Get python3 (just in case) and pip
    echo "Installing pip..."
    sudo dnf install python3 python3-pip

    # Remove old Docker versions
    echo "Removing old Docker versions..."
    sudo dnf remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine

    echo "Automagically installing latest Docker..."
    sudo dnf -y install dnf-plugins-core
    sudo dnf config-manager \
        --add-repo \
        https://download.docker.com/linux/fedora/docker-ce.repo

    sudo dnf install docker-ce docker-ce-cli containerd.io

    echo "Installing GStreamer..."
    sudo dnf install gstreamer1-devel gstreamer1-plugins-base-tools gstreamer1-doc gstreamer1-plugins-base-devel gstreamer1-plugins-good gstreamer1-plugins-good-extras gstreamer1-plugins-ugly gstreamer1-plugins-bad-free gstreamer1-plugins-bad-free-devel gstreamer1-plugins-bad-free-extras

    # Get Docker Compose
    sudo -H pip3 install docker-compose

    echo "Done installing deps! Have a nice day :)"
else
    echo "Sorry, your package manager or OS isn't supported. :/"
    echo "If you have an older Debian-based system, try installing aptitude."
fi

# Login to gitlab registry
sudo docker login registry.gitlab.com/avhs-suas/pidgin
