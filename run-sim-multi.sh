#!/bin/bash

if ! command -v docker-compose &> /dev/null; then
  echo "Docker-compose not installed."
  echo "Run ./pidgin/setup.sh to install."
  exit 1
fi

if [ "$EUID" -ne 0 ]; then
  echo "This script needs root permissions."
  exit 1
fi

xhost +
docker-compose -f pidgin/docker-compose.yml up
